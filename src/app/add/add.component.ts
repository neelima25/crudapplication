import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  employeeForm: any;
  constructor(private _todoService: TodoService, private router:Router,private formbulider: FormBuilder) { }

  ngOnInit(): void {
    this.employeeForm = this.formbulider.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required]],
    });
  };

  onFormSubmit() {
    this._todoService.addTodo(this.employeeForm.value)
    .subscribe(res => {
      alert(res.message);
      this.router.navigate(["/list"]);
    }, err => {
      console.log(err);
    });
  }

  gotoList(){
    this.router.navigate(["/list"]);
  }

}
