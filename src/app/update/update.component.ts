import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  employeeForm: any;
  id: string | null;
  constructor(private router:Router,private formbulider: FormBuilder,private _activatedRoute: ActivatedRoute, private _todoService:TodoService) { 
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
  }
 
  getbyId(id:any){
    this._todoService.getTodoById(id)
    .subscribe(res => {
      console.log(res);
      this.employeeForm.controls.firstname.setValue(res.data.firstname);
      this.employeeForm.controls.lastname.setValue(res.data.lastname);
      this.employeeForm.controls.email.setValue(res.data.email);
      
    }, err => {
      console.log(err);
    });
  }

  ngOnInit(): void {
   this.employeeForm = this.formbulider.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required]],
    });
    this.getbyId(this.id);
  };

  onFormSubmit() {
    let req =this.employeeForm.value;
    req.id = this.id;
    this._todoService.updateTodo(this.employeeForm.value)
    .subscribe(res => {
      alert(res.message);
      this.router.navigate(["/list"]);
    }, err => {
      console.log(err);
    });
  };

  gotoList(){
    this.router.navigate(["/list"]);
  }

}
