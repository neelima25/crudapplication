import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { UpdateComponent } from './update/update.component';
const routes: Routes  = [
  {
    path: '',
    component: ListComponent
  }, 
  {
    path: 'update/:id',
    component: UpdateComponent
  },
  {
    path: 'add',
    component: AddComponent
  },
  {
    path: 'list',
    component: ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
