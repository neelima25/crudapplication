import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';



export const SERVER_URL: string = "/api";

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http:HttpClient) { }

  getTodos (): Observable<any> {
    return this.http.get('http://localhost:3000/api/users/getGuetList');
  }
   
  getTodoById (id:number): Observable<any> {
    return this.http.get('http://localhost:3000/api/users/getById/'+id);
  }
  addTodo (req:any): Observable<any> {
    return this.http.post('http://localhost:3000/api/users/signup', req);
  }

  deleteTodo (index:number): Observable<any> {
    return this.http.delete('http://localhost:3000/api/users/delete/' + index)
  }
   
  updateTodo (req:any): Observable<any> {
    return this.http.put('http://localhost:3000/api/users/updateGuest', req)
  }
  // updateTodo (id, todo): Observable<any> {
  //   const url = `${apiUrl}/update.php?id=${id}`;
  //   return this.http.put(url, todo, httpOptions).pipe(
  //     tap(_ => console.log(`updated todo id=${id}`)),
  //     catchError(this.handleError<any>('updateTodo'))
  //   );
  // }
   
  // deleteTodo (id): Observable<Todo> {
  //   const url = `${apiUrl}/delete.php?id=${id}`;
   
  //   return this.http.delete<Todo>(url, httpOptions).pipe(
  //     tap(_ => console.log(`deleted todo id=${id}`)),
  //     catchError(this.handleError<Todo>('deletetodo'))
  //   );
  // }
  // private handleError<T> (operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
   
  //     // TODO: send the error to remote logging infrastructure
  //     console.error(error); // log to console instead
   
  //     // Let the app keep running by returning an empty result.
  //     return of(result as T);
  //   };
  // }
}
