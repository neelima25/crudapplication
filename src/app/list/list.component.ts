import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  displayedColumns = ['id', 'firstname','lastname','email','actions'];
  dataSource!: MatTableDataSource<any>;
  index: number | undefined;
  id: number | undefined;
  data: any;
  
 
  constructor(private _todoService: TodoService, private router:Router) { }
   
  @ViewChild(MatPaginator)
  paginator!: MatPaginator | null; 
  @ViewChild(MatSort)
  sort!: MatSort;
  ngOnInit() {
   this.getTodoList();
  };

  getTodoList(){
    this._todoService.getTodos()
    .subscribe(res => {
     this.data = res.data;
    this.dataSource = new MatTableDataSource(this.data); 
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    }, err => {
      console.log(err);
    });
  }
 
  deleteItem(index:number){ 
    this._todoService.deleteTodo(index)
    .subscribe(res => {
      alert(res.message);
      this.getTodoList();
    }, err => {
      console.log(err);
    });
    
  }; 

  applyFilter(filterValue: any) {
    this.dataSource.filter = filterValue.value.trim().toLowerCase();
    console.log(this.dataSource.filter);
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
  };

  addItem(){
    this.router.navigate(["/add"]);
  }

  editItem(index:number){
    this.router.navigate(["/update", index]);
  }

  gotoList(){
    this.router.navigate(["/list"]);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
