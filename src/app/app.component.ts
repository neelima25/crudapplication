import { TodoService } from './services/todo.service';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'interview-app';
  displayedColumns = ['id', 'title','actions'];
  dataSource!: MatTableDataSource<any>;
  index: number | undefined;
  id: number | undefined;
  data: any;
  opened: boolean | undefined;
 
  constructor(private _todoService: TodoService, private router:Router) { }
   
  ngOnInit() {
    
  };
  gotoList(){
    this.router.navigate(["/list"]);
  }
 
}
